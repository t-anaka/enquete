﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete
{
    public class JsonUtil
    {
        public static Formatting JsonFormat = Formatting.Indented;

        static public T LoadAsJson<T>(string path, T def)
        {
            if (File.Exists(path))
            {
                var json = File.ReadAllText(path);
                return JsonConvert.DeserializeObject<T>(json);
            }
            return def;
        }

        static public void SaveAsJson(string path, object obj)
        {
            var dir = Path.GetDirectoryName(path);
            if (dir != "")
            {
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
            }
            var json = JsonConvert.SerializeObject(obj, JsonFormat);
            File.WriteAllText(path, json);
        }

        static public string ToJson(object obj)
        {
            return JsonConvert.SerializeObject(obj, JsonFormat);
        }

        static public T ToObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        static public T ToObject<T>(object obj)
        {
            return ((JObject)obj).ToObject<T>();
        }
    }
}
