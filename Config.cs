﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete
{
    public class Config
    {
        public ChartType ChartType { get; set; }
        public bool Unique { get; set; }
        public bool AllMatch { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public ChartData[] Items { get; set; }

        public Config()
        {
            Reset();
        }

        public void Reset()
        {
            ChartType = ChartType.Bar;
            Unique = false;
            AllMatch = false;
            Host = "localhost";
            Port = "55022";

            var list = new List<ChartData>();
            list.Add(new ChartData
            {
                Text = "リンゴ",
                Value = 30,
                Color = Color.FromArgb(100, Color.Red),
                Border = Color.FromArgb(200, Color.Red),
            });
            list.Add(new ChartData
            {
                Text = "バナナ",
                Value = 20,
                Color = Color.FromArgb(100, Color.Yellow),
                Border = Color.FromArgb(200, Color.Orange),
            });
            list.Add(new ChartData
            {
                Text = "キウイ",
                Value = 10,
                Color = Color.FromArgb(100, Color.Green),
                Border = Color.FromArgb(200, Color.Green),
            });
            list.Add(new ChartData
            {
                Text = "",
                Value = 0,
                Color = Color.FromArgb(100, Color.Blue),
                Border = Color.FromArgb(200, Color.Blue),
            });
            list.Add(new ChartData
            {
                Text = "",
                Value = 0,
                Color = Color.FromArgb(100, Color.Orange),
                Border = Color.FromArgb(200, Color.Orange),
            });
            list.Add(new ChartData
            {
                Text = "",
                Value = 0,
                Color = Color.FromArgb(100, Color.SkyBlue),
                Border = Color.FromArgb(200, Color.SkyBlue),
            });
            list.Add(new ChartData
            {
                Text = "",
                Value = 0,
                Color = Color.FromArgb(100, Color.Pink),
                Border = Color.FromArgb(200, Color.Pink),
            });
            list.Add(new ChartData
            {
                Text = "",
                Value = 0,
                Color = Color.FromArgb(100, Color.Brown),
                Border = Color.FromArgb(200, Color.Brown),
            });
            list.Add(new ChartData
            {
                Text = "",
                Value = 0,
                Color = Color.FromArgb(100, Color.Purple),
                Border = Color.FromArgb(200, Color.Purple),
            });
            Items = list.ToArray();
        }
    }
}
