﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Enquete
{
    public partial class MainForm : Form
    {
        Config config = new Config();
        List<TextBox> texts = new List<TextBox>();
        List<Panel> panels = new List<Panel>();
        List<string> userNames = new List<string>();
        WebSocketClient client = new WebSocketClient();
        Font fontBold;
        Font fontRegular;

        public MainForm()
        {
            InitializeComponent();
            texts.Add(textBoxA);
            texts.Add(textBoxB);
            texts.Add(textBoxC);
            texts.Add(textBoxD);
            texts.Add(textBoxE);
            texts.Add(textBoxF);
            texts.Add(textBoxG);
            texts.Add(textBoxH);
            texts.Add(textBoxI);
            panels.Add(panelColorA);
            panels.Add(panelColorB);
            panels.Add(panelColorC);
            panels.Add(panelColorD);
            panels.Add(panelColorE);
            panels.Add(panelColorF);
            panels.Add(panelColorG);
            panels.Add(panelColorH);
            panels.Add(panelColorI);


            client.Connected += Client_Connected;
            client.Closed += Client_Closed;
            client.Received += Client_Received;
            client.Error += Client_Error;

            fontRegular = new Font(Font, FontStyle.Regular);
            fontBold = new Font(Font, FontStyle.Bold);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            config = JsonUtil.LoadAsJson("config.json", config);

            if (config.Items.Length != texts.Count)
                config.Reset();

            for (var i = 0; i < texts.Count; i++)
            {
                texts[i].Text = config.Items[i].Text;
                panels[i].BackColor = config.Items[i].Color;
            }

            switch (config.ChartType)
            {
                case ChartType.Bar:
                    radioChartBar.Checked = true;
                    break;
                case ChartType.Pie:
                    radioChartPie.Checked = true;
                    break;
            }

            checkUnique.Checked = config.Unique;
            checkAllMatch.Checked = config.AllMatch;

            textHost.Text = config.Host;
            textPort.Text = config.Port;

            var items = Array.FindAll(config.Items, a => a.Text.Length > 0);
            chartPanel.Datas = items;
            chartPanel.Refresh();

            buttonStart.Select();
        }

        private async void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                fontRegular.Dispose();
                fontBold.Dispose();

                if (client.IsConnected)
                    await client.Close();
            }
            finally
            {
                if (radioChartBar.Checked)
                    config.ChartType = ChartType.Bar;
                else if (radioChartPie.Checked)
                    config.ChartType = ChartType.Pie;

                config.Unique = checkUnique.Checked;
                config.AllMatch = checkAllMatch.Checked;

                JsonUtil.SaveAsJson("config.json", config);
            }
        }

        private void Client_Connected(object sender, EventArgs e)
        {
            Invoke((MethodInvoker)(() =>
            {
                buttonStart.Text = "停止";
            }));
        }

        private void Client_Closed(object sender, EventArgs e)
        {
            Invoke((MethodInvoker)(() =>
            {
                buttonStart.Text = "開始";
                labelStatus.Text = "";
                labelStatus.Font = fontRegular;
            }));
        }

        private void Client_Error(object sender, WebSocketErrorEventArgs e)
        {
            Invoke((MethodInvoker)(() =>
            {
                var msg =
                e.Exception.Message + Environment.NewLine + Environment.NewLine +
                "※リモート サーバーに接続できない場合はChatsumiを起動し外部アプリ連携をオンにしてください。" +
                "また、ホスト名とポート番号をChatsumiと同じに設定してください。";

                MsgBox.Show(this, MsgBoxIcon.Error, MsgBoxButton.OK, MsgBoxSound.Asterisk,
                    "エラー", msg);
            }));
        }

        private void UpdateData(string json)
        {
            var chat = JsonUtil.ToObject<ChatData>(json);
            if (chat == null)
                return;
            if (chat.Type != ChatType.Comment)
                return;
            var comment = JsonUtil.ToObject<CommentData>(chat.Data);
            if (comment == null)
                return;
            var i = -1;
            if (checkAllMatch.Checked)
            {
                i = Array.FindIndex(chartPanel.Datas, a => comment.Text == a.Text);
            }
            else
            {
                i = Array.FindIndex(chartPanel.Datas, a => comment.Text.Contains(a.Text));
            }
            if (i < 0)
            {
                Invoke((MethodInvoker)(() =>
                {
                    labelStatus.Font = fontRegular;
                    labelStatus.Text = $"{comment.Text}";
                }));
                return;
            }

            if (checkUnique.Checked)
            {
                if (userNames.Contains(comment.Name))
                {
                    Invoke((MethodInvoker)(() =>
                    {
                        labelStatus.Font = fontRegular;
                        labelStatus.Text = $"{comment.Text}";
                    }));
                    return;
                }
                userNames.Add(comment.Name);
            }

            chartPanel.Datas[i].Value++;
            Invoke((MethodInvoker)(() =>
            {
                labelStatus.Font = fontBold;
                labelStatus.Text = $"{comment.Text}";
                chartPanel.Refresh();

            }));
        }

        private void Client_Received(object sender, WebSocketRecvEventArgs e)
        {
            UpdateData(e.Message);
        }

        private void ButtonClearItems_Click(object sender, EventArgs e)
        {
            MsgBox.Show(this, MsgBoxIcon.Question, MsgBoxButton.OKCancel, MsgBoxSound.None,
                "確認", "すべての項目をクリアしますか？", (result) =>
                {
                    if (result != DialogResult.OK)
                        return;
                    for (var i = 0; i < texts.Count; i++)
                    {
                        texts[i].Text = "";
                    }
                });
        }

        private void ButtonSetItems_Click(object sender, EventArgs e)
        {
            MsgBox.Show(this, MsgBoxIcon.Question, MsgBoxButton.OKCancel, MsgBoxSound.None,
                "確認", "項目をグラフに設定しますか？", (result) =>
                {
                    if (result != DialogResult.OK)
                        return;
                    for (var i = 0; i < texts.Count; i++)
                    {
                        config.Items[i].Text = texts[i].Text;
                        config.Items[i].Value = 0;
                    }
                    var items = Array.FindAll(config.Items, a => a.Text.Length > 0);
                    chartPanel.Datas = items;
                    chartPanel.Refresh();
                });
        }

        private void RadioChartBar_CheckedChanged(object sender, EventArgs e)
        {
            if (radioChartBar.Checked)
            {
                chartPanel.ChartType = ChartType.Bar;
                chartPanel.Refresh();
            }
        }

        private void RadioChartPie_CheckedChanged(object sender, EventArgs e)
        {
            if (radioChartPie.Checked)
            {
                chartPanel.ChartType = ChartType.Pie;
                chartPanel.Refresh();
            }
        }

        private async void ButtonStart_Click(object sender, EventArgs e)
        {
            if (client.IsConnected)
            {
                labelStatus.Text = "切断中";
                await client.Close();
            }
            else
            {
                labelStatus.Text = "接続中";

                config.Host = textHost.Text;
                config.Port = textPort.Text;

                var uri = $"ws://{config.Host}:{config.Port}/";
                client.Connect(uri);
            }
        }

        private void CheckUnique_CheckedChanged(object sender, EventArgs e)
        {
            if (!checkUnique.Checked)
                userNames.Clear();
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            if (client.IsConnected)
            {
                MsgBox.Show(this, MsgBoxIcon.Warning, MsgBoxButton.OKCancel, MsgBoxSound.None,
                "警告", "集計中は保存できません。");
                return;
            }
            using (var bmp = new Bitmap(chartPanel.Width, chartPanel.Height))
            {
                var point = chartPanel.PointToScreen(chartPanel.Location);
                using (var g = Graphics.FromImage(bmp))
                {
                    g.CopyFromScreen(point.X, point.Y, 0, 0, chartPanel.Size);
                }
                using (var dialog = new SaveFileDialog())
                {
                    var fname = $"chart_{DateTime.Now.ToString("yyyyMMddHHmmss")}.png";

                    dialog.InitialDirectory = Application.StartupPath;
                    dialog.RestoreDirectory = true;
                    dialog.Title = "保存";
                    dialog.FileName = fname;
                    dialog.Filter = "PNG(*.png)|*.png";

                    var result = dialog.ShowDialog(this);
                    if (result != DialogResult.OK)
                        return;

                    bmp.Save(dialog.FileName, ImageFormat.Png);
                }
            }
        }

        private void ButtonClearValue_Click(object sender, EventArgs e)
        {
            MsgBox.Show(this, MsgBoxIcon.Question, MsgBoxButton.OKCancel, MsgBoxSound.None,
                "確認", "集計結果をクリアしますか？", (result) =>
                {
                    if (result == DialogResult.OK)
                    {
                        userNames.Clear();
                        for (var i = 0; i < chartPanel.Datas.Length; i++)
                        {
                            chartPanel.Datas[i].Value = 0;
                        }
                        chartPanel.Refresh();
                    }
                });
        }
    }
}
