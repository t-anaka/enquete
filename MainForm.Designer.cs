﻿namespace Enquete
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chartPanel = new Enquete.ChartPanel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupWebScoket = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textPort = new System.Windows.Forms.TextBox();
            this.textHost = new System.Windows.Forms.TextBox();
            this.labelHost = new System.Windows.Forms.Label();
            this.groupItems = new System.Windows.Forms.GroupBox();
            this.buttonSetItems = new System.Windows.Forms.Button();
            this.buttonClearItems = new System.Windows.Forms.Button();
            this.textBoxA = new System.Windows.Forms.TextBox();
            this.textBoxI = new System.Windows.Forms.TextBox();
            this.panelColorA = new System.Windows.Forms.Panel();
            this.panelColorI = new System.Windows.Forms.Panel();
            this.textBoxE = new System.Windows.Forms.TextBox();
            this.panelColorB = new System.Windows.Forms.Panel();
            this.panelColorE = new System.Windows.Forms.Panel();
            this.textBoxH = new System.Windows.Forms.TextBox();
            this.panelColorF = new System.Windows.Forms.Panel();
            this.textBoxB = new System.Windows.Forms.TextBox();
            this.textBoxD = new System.Windows.Forms.TextBox();
            this.panelColorH = new System.Windows.Forms.Panel();
            this.textBoxF = new System.Windows.Forms.TextBox();
            this.panelColorC = new System.Windows.Forms.Panel();
            this.panelColorD = new System.Windows.Forms.Panel();
            this.textBoxG = new System.Windows.Forms.TextBox();
            this.panelColorG = new System.Windows.Forms.Panel();
            this.textBoxC = new System.Windows.Forms.TextBox();
            this.groupOption = new System.Windows.Forms.GroupBox();
            this.checkAllMatch = new System.Windows.Forms.CheckBox();
            this.checkUnique = new System.Windows.Forms.CheckBox();
            this.groupChartType = new System.Windows.Forms.GroupBox();
            this.radioChartPie = new System.Windows.Forms.RadioButton();
            this.radioChartBar = new System.Windows.Forms.RadioButton();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonClearValue = new System.Windows.Forms.Button();
            this.buttonStart = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.labelStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupWebScoket.SuspendLayout();
            this.groupItems.SuspendLayout();
            this.groupOption.SuspendLayout();
            this.groupChartType.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chartPanel);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(718, 445);
            this.splitContainer1.SplitterDistance = 463;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            // 
            // chartPanel
            // 
            this.chartPanel.ChartType = Enquete.ChartType.Bar;
            this.chartPanel.Datas = null;
            this.chartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartPanel.Location = new System.Drawing.Point(0, 0);
            this.chartPanel.Name = "chartPanel";
            this.chartPanel.Size = new System.Drawing.Size(463, 445);
            this.chartPanel.SortChart = false;
            this.chartPanel.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.Controls.Add(this.groupWebScoket);
            this.splitContainer2.Panel1.Controls.Add(this.groupItems);
            this.splitContainer2.Panel1.Controls.Add(this.groupOption);
            this.splitContainer2.Panel1.Controls.Add(this.groupChartType);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.buttonSave);
            this.splitContainer2.Panel2.Controls.Add(this.buttonClearValue);
            this.splitContainer2.Panel2.Controls.Add(this.buttonStart);
            this.splitContainer2.Size = new System.Drawing.Size(251, 445);
            this.splitContainer2.SplitterDistance = 404;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // groupWebScoket
            // 
            this.groupWebScoket.Controls.Add(this.label1);
            this.groupWebScoket.Controls.Add(this.textPort);
            this.groupWebScoket.Controls.Add(this.textHost);
            this.groupWebScoket.Controls.Add(this.labelHost);
            this.groupWebScoket.Location = new System.Drawing.Point(3, 406);
            this.groupWebScoket.Name = "groupWebScoket";
            this.groupWebScoket.Size = new System.Drawing.Size(225, 68);
            this.groupWebScoket.TabIndex = 76;
            this.groupWebScoket.TabStop = false;
            this.groupWebScoket.Text = "WebSocketの設定";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "ポート";
            // 
            // textPort
            // 
            this.textPort.Location = new System.Drawing.Point(49, 40);
            this.textPort.Name = "textPort";
            this.textPort.Size = new System.Drawing.Size(100, 19);
            this.textPort.TabIndex = 18;
            // 
            // textHost
            // 
            this.textHost.Location = new System.Drawing.Point(49, 18);
            this.textHost.Name = "textHost";
            this.textHost.ReadOnly = true;
            this.textHost.Size = new System.Drawing.Size(100, 19);
            this.textHost.TabIndex = 17;
            // 
            // labelHost
            // 
            this.labelHost.AutoSize = true;
            this.labelHost.Location = new System.Drawing.Point(11, 21);
            this.labelHost.Name = "labelHost";
            this.labelHost.Size = new System.Drawing.Size(32, 12);
            this.labelHost.TabIndex = 0;
            this.labelHost.Text = "ホスト";
            // 
            // groupItems
            // 
            this.groupItems.Controls.Add(this.buttonSetItems);
            this.groupItems.Controls.Add(this.buttonClearItems);
            this.groupItems.Controls.Add(this.textBoxA);
            this.groupItems.Controls.Add(this.textBoxI);
            this.groupItems.Controls.Add(this.panelColorA);
            this.groupItems.Controls.Add(this.panelColorI);
            this.groupItems.Controls.Add(this.textBoxE);
            this.groupItems.Controls.Add(this.panelColorB);
            this.groupItems.Controls.Add(this.panelColorE);
            this.groupItems.Controls.Add(this.textBoxH);
            this.groupItems.Controls.Add(this.panelColorF);
            this.groupItems.Controls.Add(this.textBoxB);
            this.groupItems.Controls.Add(this.textBoxD);
            this.groupItems.Controls.Add(this.panelColorH);
            this.groupItems.Controls.Add(this.textBoxF);
            this.groupItems.Controls.Add(this.panelColorC);
            this.groupItems.Controls.Add(this.panelColorD);
            this.groupItems.Controls.Add(this.textBoxG);
            this.groupItems.Controls.Add(this.panelColorG);
            this.groupItems.Controls.Add(this.textBoxC);
            this.groupItems.Location = new System.Drawing.Point(3, 3);
            this.groupItems.Name = "groupItems";
            this.groupItems.Size = new System.Drawing.Size(225, 274);
            this.groupItems.TabIndex = 0;
            this.groupItems.TabStop = false;
            this.groupItems.Text = "項目";
            // 
            // buttonSetItems
            // 
            this.buttonSetItems.Location = new System.Drawing.Point(141, 243);
            this.buttonSetItems.Name = "buttonSetItems";
            this.buttonSetItems.Size = new System.Drawing.Size(75, 23);
            this.buttonSetItems.TabIndex = 12;
            this.buttonSetItems.Text = "設定";
            this.buttonSetItems.UseVisualStyleBackColor = true;
            this.buttonSetItems.Click += new System.EventHandler(this.ButtonSetItems_Click);
            // 
            // buttonClearItems
            // 
            this.buttonClearItems.Location = new System.Drawing.Point(60, 243);
            this.buttonClearItems.Name = "buttonClearItems";
            this.buttonClearItems.Size = new System.Drawing.Size(75, 23);
            this.buttonClearItems.TabIndex = 11;
            this.buttonClearItems.Text = "クリア";
            this.buttonClearItems.UseVisualStyleBackColor = true;
            this.buttonClearItems.Click += new System.EventHandler(this.ButtonClearItems_Click);
            // 
            // textBoxA
            // 
            this.textBoxA.Location = new System.Drawing.Point(31, 18);
            this.textBoxA.Name = "textBoxA";
            this.textBoxA.Size = new System.Drawing.Size(185, 19);
            this.textBoxA.TabIndex = 3;
            // 
            // textBoxI
            // 
            this.textBoxI.Location = new System.Drawing.Point(31, 218);
            this.textBoxI.Name = "textBoxI";
            this.textBoxI.Size = new System.Drawing.Size(185, 19);
            this.textBoxI.TabIndex = 11;
            // 
            // panelColorA
            // 
            this.panelColorA.BackColor = System.Drawing.Color.White;
            this.panelColorA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorA.ForeColor = System.Drawing.Color.Black;
            this.panelColorA.Location = new System.Drawing.Point(6, 18);
            this.panelColorA.Name = "panelColorA";
            this.panelColorA.Size = new System.Drawing.Size(19, 19);
            this.panelColorA.TabIndex = 64;
            // 
            // panelColorI
            // 
            this.panelColorI.BackColor = System.Drawing.Color.White;
            this.panelColorI.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorI.Location = new System.Drawing.Point(6, 218);
            this.panelColorI.Name = "panelColorI";
            this.panelColorI.Size = new System.Drawing.Size(19, 19);
            this.panelColorI.TabIndex = 72;
            // 
            // textBoxE
            // 
            this.textBoxE.Location = new System.Drawing.Point(31, 118);
            this.textBoxE.Name = "textBoxE";
            this.textBoxE.Size = new System.Drawing.Size(185, 19);
            this.textBoxE.TabIndex = 7;
            // 
            // panelColorB
            // 
            this.panelColorB.BackColor = System.Drawing.Color.White;
            this.panelColorB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorB.ForeColor = System.Drawing.Color.Black;
            this.panelColorB.Location = new System.Drawing.Point(6, 43);
            this.panelColorB.Name = "panelColorB";
            this.panelColorB.Size = new System.Drawing.Size(19, 19);
            this.panelColorB.TabIndex = 65;
            // 
            // panelColorE
            // 
            this.panelColorE.BackColor = System.Drawing.Color.White;
            this.panelColorE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorE.Location = new System.Drawing.Point(6, 118);
            this.panelColorE.Name = "panelColorE";
            this.panelColorE.Size = new System.Drawing.Size(19, 19);
            this.panelColorE.TabIndex = 68;
            // 
            // textBoxH
            // 
            this.textBoxH.Location = new System.Drawing.Point(31, 193);
            this.textBoxH.Name = "textBoxH";
            this.textBoxH.Size = new System.Drawing.Size(185, 19);
            this.textBoxH.TabIndex = 10;
            // 
            // panelColorF
            // 
            this.panelColorF.BackColor = System.Drawing.Color.White;
            this.panelColorF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorF.Location = new System.Drawing.Point(6, 143);
            this.panelColorF.Name = "panelColorF";
            this.panelColorF.Size = new System.Drawing.Size(19, 19);
            this.panelColorF.TabIndex = 69;
            // 
            // textBoxB
            // 
            this.textBoxB.Location = new System.Drawing.Point(31, 43);
            this.textBoxB.Name = "textBoxB";
            this.textBoxB.Size = new System.Drawing.Size(185, 19);
            this.textBoxB.TabIndex = 4;
            // 
            // textBoxD
            // 
            this.textBoxD.Location = new System.Drawing.Point(31, 93);
            this.textBoxD.Name = "textBoxD";
            this.textBoxD.Size = new System.Drawing.Size(185, 19);
            this.textBoxD.TabIndex = 6;
            // 
            // panelColorH
            // 
            this.panelColorH.BackColor = System.Drawing.Color.White;
            this.panelColorH.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorH.Location = new System.Drawing.Point(6, 193);
            this.panelColorH.Name = "panelColorH";
            this.panelColorH.Size = new System.Drawing.Size(19, 19);
            this.panelColorH.TabIndex = 71;
            // 
            // textBoxF
            // 
            this.textBoxF.Location = new System.Drawing.Point(31, 143);
            this.textBoxF.Name = "textBoxF";
            this.textBoxF.Size = new System.Drawing.Size(185, 19);
            this.textBoxF.TabIndex = 8;
            // 
            // panelColorC
            // 
            this.panelColorC.BackColor = System.Drawing.Color.White;
            this.panelColorC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorC.ForeColor = System.Drawing.Color.Black;
            this.panelColorC.Location = new System.Drawing.Point(6, 68);
            this.panelColorC.Name = "panelColorC";
            this.panelColorC.Size = new System.Drawing.Size(19, 19);
            this.panelColorC.TabIndex = 66;
            // 
            // panelColorD
            // 
            this.panelColorD.BackColor = System.Drawing.Color.White;
            this.panelColorD.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorD.ForeColor = System.Drawing.Color.Black;
            this.panelColorD.Location = new System.Drawing.Point(6, 93);
            this.panelColorD.Name = "panelColorD";
            this.panelColorD.Size = new System.Drawing.Size(19, 19);
            this.panelColorD.TabIndex = 67;
            // 
            // textBoxG
            // 
            this.textBoxG.Location = new System.Drawing.Point(31, 168);
            this.textBoxG.Name = "textBoxG";
            this.textBoxG.Size = new System.Drawing.Size(185, 19);
            this.textBoxG.TabIndex = 9;
            // 
            // panelColorG
            // 
            this.panelColorG.BackColor = System.Drawing.Color.White;
            this.panelColorG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelColorG.Location = new System.Drawing.Point(6, 168);
            this.panelColorG.Name = "panelColorG";
            this.panelColorG.Size = new System.Drawing.Size(19, 19);
            this.panelColorG.TabIndex = 70;
            // 
            // textBoxC
            // 
            this.textBoxC.Location = new System.Drawing.Point(31, 68);
            this.textBoxC.Name = "textBoxC";
            this.textBoxC.Size = new System.Drawing.Size(185, 19);
            this.textBoxC.TabIndex = 5;
            // 
            // groupOption
            // 
            this.groupOption.Controls.Add(this.checkAllMatch);
            this.groupOption.Controls.Add(this.checkUnique);
            this.groupOption.Location = new System.Drawing.Point(3, 333);
            this.groupOption.Name = "groupOption";
            this.groupOption.Size = new System.Drawing.Size(225, 67);
            this.groupOption.TabIndex = 10;
            this.groupOption.TabStop = false;
            this.groupOption.Text = "オプション";
            // 
            // checkAllMatch
            // 
            this.checkAllMatch.AutoSize = true;
            this.checkAllMatch.Location = new System.Drawing.Point(6, 40);
            this.checkAllMatch.Name = "checkAllMatch";
            this.checkAllMatch.Size = new System.Drawing.Size(192, 16);
            this.checkAllMatch.TabIndex = 16;
            this.checkAllMatch.Text = "項目に完全一致した場合のみ集計";
            this.checkAllMatch.UseVisualStyleBackColor = true;
            // 
            // checkUnique
            // 
            this.checkUnique.AutoSize = true;
            this.checkUnique.Location = new System.Drawing.Point(6, 18);
            this.checkUnique.Name = "checkUnique";
            this.checkUnique.Size = new System.Drawing.Size(133, 16);
            this.checkUnique.TabIndex = 15;
            this.checkUnique.Text = "投票は一人につき一回";
            this.checkUnique.UseVisualStyleBackColor = true;
            this.checkUnique.CheckedChanged += new System.EventHandler(this.CheckUnique_CheckedChanged);
            // 
            // groupChartType
            // 
            this.groupChartType.Controls.Add(this.radioChartPie);
            this.groupChartType.Controls.Add(this.radioChartBar);
            this.groupChartType.Location = new System.Drawing.Point(3, 283);
            this.groupChartType.Name = "groupChartType";
            this.groupChartType.Size = new System.Drawing.Size(225, 44);
            this.groupChartType.TabIndex = 9;
            this.groupChartType.TabStop = false;
            this.groupChartType.Text = "グラフの種類";
            // 
            // radioChartPie
            // 
            this.radioChartPie.AutoSize = true;
            this.radioChartPie.Location = new System.Drawing.Point(75, 18);
            this.radioChartPie.Name = "radioChartPie";
            this.radioChartPie.Size = new System.Drawing.Size(60, 16);
            this.radioChartPie.TabIndex = 14;
            this.radioChartPie.Text = "円グラフ";
            this.radioChartPie.UseVisualStyleBackColor = true;
            this.radioChartPie.CheckedChanged += new System.EventHandler(this.RadioChartPie_CheckedChanged);
            // 
            // radioChartBar
            // 
            this.radioChartBar.AutoSize = true;
            this.radioChartBar.Checked = true;
            this.radioChartBar.Location = new System.Drawing.Point(6, 18);
            this.radioChartBar.Name = "radioChartBar";
            this.radioChartBar.Size = new System.Drawing.Size(60, 16);
            this.radioChartBar.TabIndex = 13;
            this.radioChartBar.TabStop = true;
            this.radioChartBar.Text = "棒グラフ";
            this.radioChartBar.UseVisualStyleBackColor = true;
            this.radioChartBar.CheckedChanged += new System.EventHandler(this.RadioChartBar_CheckedChanged);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(170, 3);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(58, 30);
            this.buttonSave.TabIndex = 2;
            this.buttonSave.Text = "保存";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // buttonClearValue
            // 
            this.buttonClearValue.Location = new System.Drawing.Point(106, 3);
            this.buttonClearValue.Name = "buttonClearValue";
            this.buttonClearValue.Size = new System.Drawing.Size(58, 30);
            this.buttonClearValue.TabIndex = 1;
            this.buttonClearValue.Text = "クリア";
            this.buttonClearValue.UseVisualStyleBackColor = true;
            this.buttonClearValue.Click += new System.EventHandler(this.ButtonClearValue_Click);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(3, 3);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(97, 30);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "開始";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.ButtonStart_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.labelStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 445);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(718, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = false;
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(703, 17);
            this.labelStatus.Spring = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 467);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Enquete";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupWebScoket.ResumeLayout(false);
            this.groupWebScoket.PerformLayout();
            this.groupItems.ResumeLayout(false);
            this.groupItems.PerformLayout();
            this.groupOption.ResumeLayout(false);
            this.groupOption.PerformLayout();
            this.groupChartType.ResumeLayout(false);
            this.groupChartType.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupItems;
        private System.Windows.Forms.Button buttonSetItems;
        private System.Windows.Forms.Button buttonClearItems;
        private System.Windows.Forms.TextBox textBoxA;
        private System.Windows.Forms.TextBox textBoxI;
        private System.Windows.Forms.Panel panelColorA;
        private System.Windows.Forms.Panel panelColorI;
        private System.Windows.Forms.TextBox textBoxE;
        private System.Windows.Forms.Panel panelColorB;
        private System.Windows.Forms.Panel panelColorE;
        private System.Windows.Forms.TextBox textBoxH;
        private System.Windows.Forms.Panel panelColorF;
        private System.Windows.Forms.TextBox textBoxB;
        private System.Windows.Forms.TextBox textBoxD;
        private System.Windows.Forms.Panel panelColorH;
        private System.Windows.Forms.TextBox textBoxF;
        private System.Windows.Forms.Panel panelColorC;
        private System.Windows.Forms.Panel panelColorD;
        private System.Windows.Forms.TextBox textBoxG;
        private System.Windows.Forms.Panel panelColorG;
        private System.Windows.Forms.TextBox textBoxC;
        private System.Windows.Forms.GroupBox groupOption;
        private System.Windows.Forms.CheckBox checkAllMatch;
        private System.Windows.Forms.CheckBox checkUnique;
        private System.Windows.Forms.GroupBox groupChartType;
        private System.Windows.Forms.RadioButton radioChartPie;
        private System.Windows.Forms.RadioButton radioChartBar;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox groupWebScoket;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private ChartPanel chartPanel;
        private System.Windows.Forms.ToolStripStatusLabel labelStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textPort;
        private System.Windows.Forms.TextBox textHost;
        private System.Windows.Forms.Label labelHost;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonClearValue;
    }
}

