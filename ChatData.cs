﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enquete
{
    public enum ChatType
    {
        Connect, Comment
    }

    public enum CommentType
    {
        General, Member, Moderator, Owner, SuperChat, Membership
    }

    public class ChatData
    {
        public ChatType Type { get; set; }
        public object Data { get; set; }
    }

    public class ConnectData
    {
        public DateTime Time { get; set; }
        public string Id { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
    }

    public class CommentData
    {
        public DateTime Time { get; set; }
        public CommentType Type { get; set; }
        public string Id { get; set; }
        public string Photo { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        public string Html { get; set; }
        public string Amount { get; set; } //for SuperChat
        public string Event { get; set; }  //for Membership
    }
}
