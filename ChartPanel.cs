﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Enquete
{
    public enum ChartType
    {
        Bar, Pie
    }

    public struct ChartData
    {
        public Color Color { get; set; }
        public Color Border { get; set; }
        public string Text { get; set; }
        public int Value { get; set; }
    }

    public class ChartPanel : Panel
    {
        public ChartType ChartType { get; set; } = ChartType.Bar;
        public ChartData[] Datas { get; set; } = null;
        public bool SortChart { get; set; } = false;

        public ChartPanel()
        {
            SetStyle(ControlStyles.ResizeRedraw, true);
            DoubleBuffered = true;
            Paint += Panel_Paint;
        }

        private void Panel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(Color.White);
            if (Datas == null)
                return;
            if (Datas.Length == 0)
                return;
            switch (ChartType)
            {
                case ChartType.Bar:
                    BarChart(e.Graphics);
                    break;
                case ChartType.Pie:
                    PieChart(e.Graphics);
                    break;
            }
        }

        private void BarChart(Graphics g)
        {
            var pad = new Padding(80, 80, 80, 80);

            var client = Rectangle.FromLTRB(
                ClientRectangle.Left + pad.Left,
                ClientRectangle.Top + pad.Top,
                ClientRectangle.Right - pad.Right,
                ClientRectangle.Bottom - pad.Bottom);

            if (client.Height < 1 || client.Width < 1)
                return;

            var max = Datas.Max(a => a.Value);
            var total = Datas.Sum(a => a.Value);

            var datas = new List<ChartData>(Datas);
            if (SortChart)
                datas.Sort((a, b) => b.Value - a.Value);

            //Y軸
            var limit = 10;
            //if (limit < max)
            //{
            //    limit = int.Parse($"10{new string('0', max.ToString().Length - 1)}");
            //    if (limit / 2 >= max)
            //        limit = limit / 2;
            //}
            if (limit < max)
            {
                limit = int.Parse($"10{new string('0', max.ToString().Length - 1)}");
                if (limit / 4 >= max)
                    limit = limit / 4;
                else if (limit / 2 >= max)
                    limit = limit / 2;
            }

            var linePen = new Pen(Color.FromArgb(50, Color.Black));
            var numBrush = new SolidBrush(Color.FromArgb(150, Color.Black));

            var stepYCount = 5;
            var stepYValue = limit / stepYCount;
            var stepH = (float)client.Height / stepYCount;
            for (var i = 0; i <= stepYCount; i++)
            {
                var y = client.Top + i * stepH;

                if (i < stepYCount)
                    g.DrawLine(linePen, client.Left, y, client.Right, y);

                var label = $"{(stepYCount - i) * stepYValue}";
                var labelSize = g.MeasureString(label, Font);
                var labelX = client.Left - labelSize.Width - 10;
                var labelY = y - labelSize.Height * 0.5f;
                g.DrawString(label, Font, numBrush, labelX, labelY);
            }

            //グラフ
            var labelBrush = new SolidBrush(Color.FromArgb(150, Color.Black));
            var valueBrush = new SolidBrush(Color.FromArgb(150, Color.Black));
            var labelMaxWidth = datas.Max(a => g.MeasureString(a.Text, Font).Width) + 4;
            var stepXCount = datas.Count;
            var stepW = (float)client.Width / stepXCount;
            var barW = stepW * 0.6f;
            for (var i = 0; i <= stepXCount; i++)
            {
                var x = client.Left + i * stepW;
                //g.DrawLine(linePen, x, client.Top, x, client.Bottom);

                if (i < stepXCount)
                {
                    var barPen = new Pen(datas[i].Border);
                    var barBrush = new SolidBrush(datas[i].Color);
                    var barH = (float)datas[i].Value / limit * client.Height;
                    var barX = x + stepW * 0.5f - barW * 0.5f;
                    var barY = client.Top + client.Height - barH;
                    g.FillRectangle(barBrush, barX, barY, barW, barH);
                    g.DrawRectangle(barPen, barX, barY, barW, barH);

                    if (total > 0)
                    {
                        var val = $"{datas[i].Value}";
                        var valSize = g.MeasureString(val, Font);
                        var valX = barX + barW * 0.5f - valSize.Width * 0.5f;
                        var valY = barY - valSize.Height - 4;
                        g.DrawString(val, Font, valueBrush, valX, valY);
                    }

                    var labelSize = g.MeasureString(datas[i].Text, Font);
                    if (labelMaxWidth < stepW)
                    {
                        var labelX = x + stepW * 0.5f - labelSize.Width * 0.5f;
                        var labelY = client.Bottom + 4;
                        g.DrawString(datas[i].Text, Font, labelBrush, labelX, labelY);
                    }
                    else
                    {
                        var labelX = x + stepW * 0.5f;
                        var labelY = client.Bottom + 4;

                        g.TranslateTransform(labelX, labelY);
                        g.ScaleTransform(1.0f, 1.0f);
                        g.RotateTransform(45);
                        g.DrawString(datas[i].Text, Font, labelBrush, 0, 0);
                        g.ResetTransform();
                    }
                }
            }

            var underLinePen = new Pen(Color.FromArgb(150, Color.Black));
            g.DrawLine(underLinePen, client.Left, client.Bottom, client.Right, client.Bottom);
        }


        private (PointF, PointF) GetPoint(Rectangle client, float r, float angle)
        {
            var cx = client.Left + client.Width * 0.5f;
            var cy = client.Top + client.Height * 0.5f;
            var radian = angle * Math.PI / 180.0;
            var x = (float)(Math.Cos(radian) * r + cx);
            var y = (float)(Math.Sin(radian) * r + cy);
            return (new PointF(cx, cy), new PointF(x, y));
        }

        private void PieChart(Graphics g)
        {
            var pad = new Padding(80, 80, 80, 80);

            var client = Rectangle.FromLTRB(
                ClientRectangle.Left + pad.Left,
                ClientRectangle.Top + pad.Top,
                ClientRectangle.Right - pad.Right,
                ClientRectangle.Bottom - pad.Bottom);

            var side = Math.Min(client.Width, client.Height);
            client.X = (int)(ClientSize.Width * 0.5f - side * 0.5f);
            client.Y = (int)(ClientSize.Height * 0.5f - side * 0.5f);
            client.Width = side;
            client.Height = side;

            if (client.Height < 1 || client.Width < 1)
                return;

            var max = Datas.Max(a => a.Value);
            var total = Datas.Sum(a => a.Value);
            var datas = new List<ChartData>(Datas);

            if (SortChart)
                datas.Sort((a, b) => b.Value - a.Value);

            var ratios = new List<float>();
            var sweeps = new List<float>();
            var starts = new List<float>();
            starts.Add(-90);
            if (total == 0)
            {
                for (var i = 0; i < datas.Count; i++)
                {
                    ratios.Add(1.0f / datas.Count);
                    sweeps.Add(ratios[i] * 360f);
                    if (i < datas.Count - 1)
                        starts.Add(starts[i] + sweeps[i]);
                }
            }
            else
            {
                for (var i = 0; i < datas.Count; i++)
                {
                    ratios.Add((float)datas[i].Value / total);
                    sweeps.Add(ratios[i] * 360f);
                    g.FillPie(new SolidBrush(datas[i].Color), client, starts[i], sweeps[i]);
                    if (i < datas.Count - 1)
                        starts.Add(starts[i] + sweeps[i]);
                }
            }
            g.DrawEllipse(new Pen(Color.FromArgb(150, Color.Black)), client);

            var rects = new List<RectangleF>();
            var labelR = client.Width * 0.55f;
            var valueR = client.Width * 0.28f;
            for (var i = 0; i < datas.Count; i++)
            {
                var angle = starts[i] + sweeps[i] * 0.5f;
                var add = 0;
                if (total > 0)
                {
                    var value = $"{ratios[i] * 100:0.0}%";
                    var valueSize = g.MeasureString(value, Font);
                    var valueRect = RectangleF.Empty;
                    while (true)
                    {
                        (var pt1, var pt2) = GetPoint(client, valueR + add, angle);
                        var valueX = pt2.X - valueSize.Width * 0.5f;
                        var valueY = pt2.Y - valueSize.Height * 0.5f;
                        valueRect = new RectangleF(new PointF(valueX, valueY), valueSize);
                        if (!rects.Exists(a => a.IntersectsWith(valueRect)))
                            break;
                        add++;
                    }
                    g.DrawString(value, Font, Brushes.Black, valueRect.X, valueRect.Y);
                    rects.Add(valueRect);
                }

                var label = $"{datas[i].Text}";
                var labelSize = g.MeasureString(label, Font);
                add = 0;
                var labelRect = RectangleF.Empty;
                while (true)
                {
                    (var pt1, var pt2) = GetPoint(client, labelR + add, angle);
                    var labelX = pt2.X - labelSize.Width * 0.5f;
                    var labelY = pt2.Y - labelSize.Height * 0.5f;
                    labelRect = new RectangleF(new PointF(labelX, labelY), labelSize);
                    if (!rects.Exists(a => a.IntersectsWith(labelRect)))
                        break;
                    add++;
                }
                g.DrawString(label, Font, Brushes.Black, labelRect.X, labelRect.Y);
                rects.Add(labelRect);
            }
        }
    }
}
